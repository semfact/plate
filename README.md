
# Plate

Plate is a
[SHI3LD](http://wimmics.inria.fr/projects/shi3ld/)
implementation in Java using the
[Apache Jena Framework](http://jena.apache.org/).

The current implementation is far from complete, but basic query manipulation
is possible.

## Usage

To shield a SPARQL query we need an access control graph using the
[S4AC ontology](http://ns.inria.fr/s4ac/v2/s4ac_v2.html) and
a context graph using the
[PRISSMA ontology](http://ns.inria.fr/prissma/v1/prissma_v1.html).

### Alice, Bob and Carol

For a basic illustration we use an access control graph that allows `Alice` to
access the graph `AliceGraph` and `Bob` to access the graph `BobGraph`.
See
[access.ttl](https://github.com/semfact/plate/blob/master/src/main/resources/eu/semfact/plate/example1/access.ttl)
for a complete listing.

These excerpts from
[Example1](https://github.com/semfact/plate/blob/master/src/main/java/eu/semfact/plate/example/Example1.java)
should illustrate the basic usage of Plate.

### Plating

Create a new Plate instance and set the access control graph

```java
Plate plate = new Plate();
plate.setAccessControl(access);
```

and add a simple SPARQL query like

```java
String query = "SELECT ?x ?y ?z WHERE { ?x ?y ?z }";
```

#### Alice
A query from Alice with context

```turtle
ex:AliceContext a prissma:Context ;
                prissma:user ex:Alice .
```

using Plate

```java
plate.setContext(contextGraphAlice);
plate.setContextNode(AliceContext);
plate.refresh();
plate.shieldSparql(query);
```

would give us

```plaintext
SELECT  ?x ?y ?z
FROM NAMED <http://example.com/AliceGraph>
WHERE
  { ?x ?y ?z }
```

#### Bob

A query from Bob with context

```turtle
ex:BobContext a prissma:Context ;
              prissma:user ex:Bob .
```

```java
plate.setContext(contextGraphBob);
plate.setContextNode(BobContext);
plate.refresh();
plate.shieldSparql(query);
```

would give us

```plaintext
SELECT  ?x ?y ?z
FROM NAMED <http://example.com/BobGraph>
WHERE
  { ?x ?y ?z }
```

#### Carol

A query from Carol with context

```turtle
ex:CarolContext a prissma:Context ;
                prissma:user ex:Carol .
```

```java
plate.setContext(contextGraphCarol);
plate.setContextNode(CarolContext);
plate.refresh();
plate.shieldSparql(query);
```

would give us

```plaintext
null
```

which means that Carol is not allowed to execute her query.

## License

The MIT License

```plaintext
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
```
