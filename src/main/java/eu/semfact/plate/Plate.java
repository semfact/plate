
package eu.semfact.plate;
/*
 * #%L
 * Plate
 * %%
 * Copyright (C) 2012 Niclas Hoyer, Fiona Schmidtke
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.sparql.core.Quad;
import com.hp.hpl.jena.sparql.modify.request.UpdateData;
import com.hp.hpl.jena.update.Update;
import com.hp.hpl.jena.update.UpdateFactory;
import com.hp.hpl.jena.update.UpdateRequest;
import eu.semfact.plate.endpoint.EndpointFactory;
import eu.semfact.plate.s4ac.crud.CRUD;
import eu.semfact.plate.s4ac.crud.QueryCRUDMapper;
import eu.semfact.plate.s4ac.sparql.AccessPolicySelector;
import java.util.Collection;

public class Plate extends AccessPolicySelector {
    
    public Plate() {
        super();
    }
    
    public void setContext(String uri) {
        this.setContext(EndpointFactory.fromURI(uri));
    }
    
    public void setContext(Model model) {
        this.setContext(EndpointFactory.fromModel(model));
    }
    
    public void setAccessControl(String uri) {
        this.setAccessControl(EndpointFactory.fromURI(uri));
    }
    
    public void setAccessControl(Model model) {
        this.setAccessControl(EndpointFactory.fromModel(model));
    }
    
    public Collection<Node> getGrantedResources(Query query) {
        CRUD op = QueryCRUDMapper.map(query);
        return this.getGrantedResources(op);
    }
    
    public Collection<Node> getGrantedResources(UpdateRequest query) {
        CRUD op = QueryCRUDMapper.map(query);
        return this.getGrantedResources(op);
    }
	
	public Collection<String> getGrantedURIs(Query query) {
		CRUD op = QueryCRUDMapper.map(query);
		return this.getGrantedURIs(op);
	}
	
	public Collection<String> getGrantedURIs(UpdateRequest query) {
		CRUD op = QueryCRUDMapper.map(query);
		return this.getGrantedURIs(op);
	}
    
    public Query shield(Query query) {
        Collection<Node> nodes = this.getGrantedResources(query);
		Collection<String> uris = this.getGrantedURIs(query);
        
        if (nodes.isEmpty()) {
            return null;
        }
        
        if (!query.getNamedGraphURIs().isEmpty()) {
            if (uris.containsAll(query.getNamedGraphURIs())) {
                return query;
            } else {
                return null;
            }
        } else {
            for(Node n : nodes) {
                query.addNamedGraphURI(n.getURI());
            }
            return query;
        }
    }
    
    public UpdateRequest shield(UpdateRequest query) {
        Collection<Node> nodes = this.getGrantedResources(query);
        
        if (nodes.isEmpty()) {
            return null;
        }
        
        UpdateData data;
        
        for (Update u : query.getOperations()) {
            if (u instanceof UpdateData) {
                data = (UpdateData) u;
                for (Quad q : data.getQuads()) {
                    if (!nodes.contains(q.getGraph())) {
                        return null;
                    }
                }
            } else {
                return null;
            }
        }
        
        return query;
    }
    
    public String shieldSparql(String query) {
        Query q = QueryFactory.create(query);
        q = this.shield(q);
        if (q != null) {
            return q.toString();
        } else {
            return null;
        }
    }
    
    public String shieldSparqlUpdate(String query) {
        UpdateRequest q = UpdateFactory.create(query);
        q = this.shield(q);
        if (q != null) {
            return q.toString();
        } else {
            return null;
        }
    }
    
    public boolean isAllowed(Node node, CRUD operation) {
        return this.allowedResources.get(operation).contains(node);
    }
    
    public boolean isAllowed(String uri, CRUD operation) {
        Node node = Node.createURI(uri);
        return this.isAllowed(node, operation);
    }
    
    public boolean isAllowed(String uri, String operation) {
        CRUD op = QueryCRUDMapper.toCRUD(operation);
        return this.isAllowed(uri, op);
    }
    
}
