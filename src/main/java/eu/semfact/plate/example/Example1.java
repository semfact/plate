/*
 * #%L
 * Plate
 * %%
 * Copyright (C) 2012 Niclas Hoyer, Fiona Schmidtke
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

package eu.semfact.plate.example;

import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.util.FileManager;
import eu.semfact.plate.Plate;


public class Example1  {
    
    private static Model loadTurtle(String file) {
        return FileManager.get().loadModel("eu/semfact/plate/example1/"+file, null, "TURTLE");
    }
    
    public static void main( String[] args ) {
        
        Model access   = loadTurtle("access.ttl");
        
        Model contextGraphAlice = loadTurtle("contextAlice.ttl");
        Model contextGraphBob = loadTurtle("contextBob.ttl");
        Model contextGraphCarol = loadTurtle("contextCarol.ttl");
        
        Node AliceContext = Node.createURI("http://example.com/AliceContext");
        Node BobContext   = Node.createURI("http://example.com/BobContext");
        Node CarolContext = Node.createURI("http://example.com/CarolContext");
        
        Plate plate = new Plate();
        plate.setAccessControl(access);
        
        String query = "SELECT ?x ?y ?z WHERE { ?x ?y ?z }";
        
        plate.setContext(contextGraphAlice);
        plate.setContextNode(AliceContext);
        plate.refresh();
        System.out.println("Query for Alice:");
        System.out.println(plate.shieldSparql(query));
        
        plate.setContext(contextGraphBob);
        plate.setContextNode(BobContext);
        plate.refresh();
        System.out.println("Query for Bob:");
        System.out.println(plate.shieldSparql(query));
        
        plate.setContext(contextGraphCarol);
        plate.setContextNode(CarolContext);
        plate.refresh();
        System.out.println("Query for Carol:");
        System.out.println(plate.shieldSparql(query));
    }
    
}
