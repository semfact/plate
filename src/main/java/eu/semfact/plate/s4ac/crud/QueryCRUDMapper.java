package eu.semfact.plate.s4ac.crud;
/*
 * #%L
 * Plate
 * %%
 * Copyright (C) 2012 Niclas Hoyer, Fiona Schmidtke
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.sparql.modify.request.UpdateDataDelete;
import com.hp.hpl.jena.sparql.modify.request.UpdateDataInsert;
import com.hp.hpl.jena.sparql.modify.request.UpdateModify;
import com.hp.hpl.jena.update.Update;
import com.hp.hpl.jena.update.UpdateRequest;
import eu.semfact.plate.s4ac.Namespace;

public class QueryCRUDMapper {

    public static CRUD map(Query query) {
        return CRUD.Read;
    }

    public static CRUD map(UpdateRequest query) {
        CRUD op = CRUD.Update;
        for (Update u : query.getOperations()) {
            if (u instanceof UpdateDataInsert) {
                op = CRUD.Create;
            } else if (u instanceof UpdateModify) {
                op = CRUD.Update;
            } else if (u instanceof UpdateDataDelete) {
                op = CRUD.Delete;
            }
        }
        return op;
    }

    public static String toString(CRUD op) {
        switch (op) {
            case Create:
                return Namespace.S4AC + "Create";
            case Read:
                return Namespace.S4AC + "Read";
            case Update:
                return Namespace.S4AC + "Upate";
            case Delete:
                return Namespace.S4AC + "Delete";
        }
        return null;
    }
    
    public static CRUD toCRUD(String op) {
        switch(op) {
            case Namespace.S4AC+"Create":
                return CRUD.Create;
            case Namespace.S4AC+"Read":
                return CRUD.Read;
            case Namespace.S4AC+"Update":
                return CRUD.Update;
            case Namespace.S4AC+"Delete":
                return CRUD.Delete;
        }
        return null;
    }
}
