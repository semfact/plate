package eu.semfact.plate.s4ac.sparql;
/*
 * #%L
 * Plate
 * %%
 * Copyright (C) 2012 Niclas Hoyer, Fiona Schmidtke
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.ResultSetFormatter;
import com.hp.hpl.jena.rdf.model.RDFNode;
import eu.semfact.plate.endpoint.Endpoint;
import eu.semfact.plate.s4ac.AccessConditionSet;
import eu.semfact.plate.s4ac.AccessPolicy;
import eu.semfact.plate.s4ac.ConjunctiveAccessConditionSet;
import eu.semfact.plate.s4ac.DisjunctiveAccessConditionSet;
import eu.semfact.plate.s4ac.Namespace;
import eu.semfact.plate.s4ac.crud.CRUD;
import eu.semfact.plate.s4ac.crud.QueryCRUDMapper;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;

public class AccessPolicySelector {

    protected Endpoint accesscontrol;
    protected Endpoint context;
	protected Node contextNode;
    protected Map<CRUD, Collection<Node>> allowedResources;

    public AccessPolicySelector() {
		this.allowedResources = this.getEmptyAllowedResources();
    }
	
	private Map<CRUD, Collection<Node>> getEmptyAllowedResources() {
		Collection<Node> nodes;
		Map<CRUD, Collection<Node>> allowed;
		allowed = new HashMap<>();
		for (CRUD op : CRUD.values()) {
			nodes = new HashSet<>();
			allowed.put(op, nodes);
		}
		return allowed;
	}

    public void setAccessControl(Endpoint ep) {
        this.accesscontrol = ep;
    }

    public void setContext(Endpoint ep) {
        this.context = ep;
    }
	
	public void setContextNode(Node node) {
		this.contextNode = node;
	}

	public Endpoint getAccessControl() {
		return accesscontrol;
	}

	public Endpoint getContext() {
		return context;
	}

	public Node getContextNode() {
		return contextNode;
	}

	public Collection<Node> getGrantedResources(CRUD operation) {
		return this.allowedResources.get(operation);
	}
	
    public Collection<Node> getGrantedResources(String operation) {
		CRUD op = QueryCRUDMapper.toCRUD(operation);
        return this.getGrantedResources(op);
    }
	
	public Collection<String> getGrantedURIs(CRUD operation) {
		Collection<Node> nodes = this.getGrantedResources(operation);
		Collection<String> strs = new HashSet<>();
		for (Node node : nodes) {
			strs.add(node.getURI());
		}
		return strs;
	}
	
	public Collection<String> getGrantedURIs(String operation) {
		CRUD op = QueryCRUDMapper.toCRUD(operation);
		return this.getGrantedURIs(op);
	}

    public void refresh() {
		
		Map<RDFNode, AccessPolicy> policies = new HashMap<>();
		Map<RDFNode, AccessConditionSet> conditionSets = new HashMap<>();
        Query query;
		ResultSet results;
		QuerySolution s;
		String mode;
		
		query = QueryFactory.read("src/main/resources/eu/semfact/plate/s4ac/sparql/policies.sparql");
        results = this.accesscontrol.getService(query).execSelect();
		
		while(results.hasNext()) {
			s = results.next();
			mode = s.getLiteral("m").getString();
			switch(mode) {
				case "1":
					this.addAccessPolicy(s, policies);
				break;
				case "2":
					this.addPrivilege(s, policies);
				break;
				case "3":
					this.addAccessConditionSet(s, conditionSets, policies);
				break;
				case "4":
					this.addQuery(s, conditionSets);
				break;
			}
		}
		
		this.allowedResources = this.getAllowedResources(policies.values());

    }
	
	protected void addAccessPolicy(QuerySolution s, Map<RDFNode, AccessPolicy> m) {
		RDFNode rdfpolicy;
		RDFNode rdftitle;
		RDFNode rdfprotected;
		Node node;
		
		rdfprotected = s.get("protected");
		if (rdfprotected.isURIResource()) {
			rdfpolicy = s.get("p");
			rdftitle = s.get("title");
			AccessPolicy p = new AccessPolicy();
			if (rdftitle != null && rdftitle.isLiteral()) {
				p.setTitle(rdftitle.asLiteral().getString());
			}
			node = Node.createURI(rdfprotected.asResource().getURI());
			p.setProtectedResource(node);
			m.put(rdfpolicy, p);
		}
	}
	
	protected void addPrivilege(QuerySolution s, Map<RDFNode, AccessPolicy> m) {
		RDFNode rdfpolicy;
		RDFNode rdfprivilege;
		String privstr;
		CRUD priv;
		
		rdfpolicy = s.get("p");
		rdfprivilege = s.get("privilege");
		AccessPolicy p = m.get(rdfpolicy);
		if (p != null && rdfprivilege.isURIResource()) {
			privstr = rdfprivilege.asResource().getURI();
			priv = QueryCRUDMapper.toCRUD(privstr);
			if (priv != null) {
				p.addPrivilege(priv);
			}
		}
	}
	
	protected void addAccessConditionSet(QuerySolution s, Map<RDFNode, AccessConditionSet> m, Map<RDFNode, AccessPolicy> m2) {
		RDFNode rdfpolicy;
		RDFNode rdfacs;
		RDFNode rdftype;
		String type;
		AccessConditionSet acs;
		
		rdfpolicy = s.get("p");
		AccessPolicy p = m2.get(rdfpolicy);
		acs = null;
		
		if(p != null) {
			rdfacs = s.get("acs");
			rdftype = s.get("type");
			if (rdfacs.isResource() && rdftype.isURIResource()) {
				type = rdftype.asResource().getURI();
				switch(type) {
					case Namespace.S4AC+"ConjunctiveAccessConditionSet":
						acs = new ConjunctiveAccessConditionSet();
					break;
					case Namespace.S4AC+"DisjunctiveAccessConditionSet":
						acs = new DisjunctiveAccessConditionSet();
					break;
				}
				if (acs != null) {
					p.addAccessConditionSet(acs);
					m.put(rdfacs, acs);
				}
			}
		}
	}
	
	protected void addQuery(QuerySolution s, Map<RDFNode, AccessConditionSet> m) {
		RDFNode rdfacs;
		RDFNode rdfquery;
		AccessConditionSet acs;
		String querystr;
		Query query;
		
		rdfacs = s.get("acs");
		acs = m.get(rdfacs);
		
		if (acs != null) {
			rdfquery = s.get("query");
			if (rdfquery.isLiteral()) {
				querystr = rdfquery.asLiteral().getString();
				try {
					query = QueryFactory.create(querystr);
					acs.add(query);
				} catch (Exception e) {
				}
			}
		}
	}
	
	protected Map<CRUD, Collection<Node>> getAllowedResources(Collection<AccessPolicy> policies) {
		Node node;
		Collection<Node> nodes;
		Map<CRUD, Collection<Node>> allowed;
		
		allowed = getEmptyAllowedResources();
		
		this.filterAccessPolicies(policies);
		for (AccessPolicy p : policies) {
			node = p.getProtectedResource();
			for (CRUD op : p.getPrivileges()) {
				nodes = allowed.get(op);
				nodes.add(node);
			}
		}
		
		return allowed;
	}
	
	protected void filterAccessPolicies(Collection<AccessPolicy> policies) {
        Collection<AccessPolicy> rem = new LinkedList<>();
		boolean ask;
		for (AccessPolicy p : policies) {
			if (contextNode != null) {
				p.bind(contextNode);
			}
			ask = p.eval(context);
			if (!ask) {
				rem.add(p);
			}
		}
        policies.removeAll(rem);
	}
}
