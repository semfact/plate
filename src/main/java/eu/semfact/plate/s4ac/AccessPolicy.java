
package eu.semfact.plate.s4ac;
/*
 * #%L
 * Plate
 * %%
 * Copyright (C) 2012 Niclas Hoyer, Fiona Schmidtke
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.hp.hpl.jena.graph.Node;
import eu.semfact.plate.endpoint.Endpoint;
import eu.semfact.plate.s4ac.crud.CRUD;
import java.util.Collection;
import java.util.HashSet;

public class AccessPolicy {
    protected String title;
    protected Node protectedResource;
    protected Collection<AccessConditionSet> accessConditions;
    protected Collection<CRUD> privileges;
    
    public AccessPolicy() {
        this.accessConditions = new HashSet<>();
        this.privileges = new HashSet<>();
    }
    
    public void addAccessConditionSet(AccessConditionSet acs) {
        this.accessConditions.add(acs);
    }
    
    public boolean eval(Endpoint ep) {
        for (AccessConditionSet acs : accessConditions) {
            if (acs.eval(ep)) {
                return true;
            }
        }
        return false;
    }
    
    public void bind(Node context) {
        for (AccessConditionSet acs : accessConditions) {
            acs.bind(context);
        }
    }
    
    public void setProtectedResource(Node protectedResource) {
        this.protectedResource = protectedResource;
    }
    
    public Node getProtectedResource() {
        return this.protectedResource;
    }
    
    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }
    
    public Collection<AccessConditionSet> getAccessConditionSets() {
        return this.accessConditions;
    }
    
    public Collection<CRUD> getPrivileges() {
        return this.privileges;
    }
    
    public boolean addPrivilege(CRUD priv) {
        return this.privileges.add(priv);
    }
}
 