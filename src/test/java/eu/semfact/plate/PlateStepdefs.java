package eu.semfact.plate;
/*
 * #%L
 * Plate
 * %%
 * Copyright (C) 2012 Niclas Hoyer, Fiona Schmidtke
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.update.UpdateAction;
import com.hp.hpl.jena.update.UpdateFactory;
import com.hp.hpl.jena.update.UpdateRequest;
import com.hp.hpl.jena.util.FileManager;
import cucumber.annotation.en.Given;
import cucumber.annotation.en.Then;
import cucumber.runtime.PendingException;
import eu.semfact.plate.s4ac.crud.CRUD;
import eu.semfact.plate.s4ac.crud.QueryCRUDMapper;
import java.util.Collection;
import org.junit.Assert;

public class PlateStepdefs {

    Model data;
    Model accessControl;
    Model context;
    Plate plate;
    
    public PlateStepdefs() {
        FileManager.get().addLocatorClassLoader(PlateStepdefs.class.getClassLoader());
        accessControl = ModelFactory.createDefaultModel();
        context       = ModelFactory.createDefaultModel();
        plate = new Plate();
        plate.setAccessControl(accessControl);
        plate.setContext(context);
    }
    
    @Given("^I have some data at \"([^\"]*)\"$")
    public void I_have_some_data_at_graph(String graph) throws Throwable {
        data = FileManager.get().loadModel("eu/semfact/plate/example.ttl", "http://example.com/"+graph, "TURTLE");
    }

    @Given("^I allow ([^ ]*) access to anyone at \"([^\"]*)\"$")
    public void I_allow_read_access_to_anyone_at(String crud, String graph) throws Throwable {
        String s4acop = QueryCRUDMapper.toString(toCrud(crud));
        if (s4acop != null) {
            String query = ""
                +"PREFIX s4ac: <http://ns.inria.fr/s4ac/v2#>\n\n"
                +"INSERT DATA {\n"
                +"    _:p a s4ac:AccessPolicy ;\n"
                +"        s4ac:appliesTo <http://example.com/"+graph+"> ;\n"
                +"        s4ac:hasAccessPrivilege [a <"+s4acop+">] ;\n"
                +"        s4ac:hasAccessConditionSet _:acs .\n"
                +"    _:acs a s4ac:AccessConditionSet ;\n"
                +"          a s4ac:ConjunctiveAccessConditionSet ;\n"
                +"          s4ac:hasAccessCondition _:ac1 .\n"
                +"    _:ac1 a s4ac:AccessCondition ;\n"
                +"          s4ac:hasQueryAsk\n"
                +"\"\"\"PREFIX prissma: <http://ns.inria.fr/prissma/v1#>\n"
                +"ASK {\n"
                +"    ?context a prissma:Context ;\n"
                +"             prissma:user ?u .\n"
                +"}\"\"\" .\n"
                +"}";
            UpdateAction.parseExecute(query, accessControl);
        } else {
            throw new PendingException("Operation "+crud+" not implemented yet");
        }
    }

    @Then("^anyone should be able to read from \"([^\"]*)\"$")
    public void anyone_should_be_able_to_read_from(String graph) throws Throwable {
        this.initContext();
		plate.refresh();
        Query q = QueryFactory.create("SELECT ?x ?y ?z WHERE {?x ?y ?z}");
        Query sq = plate.shield(q);
        
        Assert.assertNotNull("Query should be allowed", sq);
        
        Collection<String> uris = sq.getNamedGraphURIs();

        Assert.assertTrue("Protected resource is in FROM NAMED section", uris.contains("http://example.com/"+graph));
        
    }

    @Then("^noone should be able to update \"([^\"]*)\"$")
    public void noone_should_be_able_to_op(String graph) throws Throwable {
        this.initContext();
		plate.refresh();
        UpdateRequest q  = UpdateFactory.create("INSERT DATA { GRAPH <http://example.com/"+graph+"> { _:t a <http://example.com/test> } }");
        UpdateRequest sq = plate.shield(q);
        
        Assert.assertNull("Update query should be null (hence not allowed)", sq);
    }
    
    protected void initContext() {
        context = ModelFactory.createDefaultModel();
        plate.setContext(context);
        String query = ""
            +"PREFIX prissma: <http://ns.inria.fr/prissma/v1#>\n\n"
            +"INSERT DATA {\n"
            +"    _:c a prissma:Context ;\n"
            +"        prissma:user _:u .\n"
            +"}";
        UpdateAction.parseExecute(query, context);
    }
    
    protected CRUD toCrud(String crud) {
        crud = crud.toLowerCase();
        switch (crud) {
            case "create":
                return CRUD.Create;
            case "read":
                return CRUD.Read;
            case "update":
                return CRUD.Update;
            case "delete":
                return CRUD.Delete;
        }
        return null;
    }
}
